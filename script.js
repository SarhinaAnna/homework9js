function renderList(array, parent = document.body) {
  let ul = document.createElement("ul");
  for (let i = 0; i < array.length; i++) {
    let li = document.createElement("li");
    li.textContent = array[i];
    ul.append(li);
  }
  parent.append(ul);
}

renderList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);
